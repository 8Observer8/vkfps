﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UsersGet : MonoBehaviour
{
    public InputField TextEditID;
    public Button btnID;
    public Text output;
    public Text txtFullName;

    private UserData userData;
    private int id;

    public void btnID_ButtonClick()
    {
        if (string.IsNullOrEmpty(TextEditID.text))
        {
            output.text = "Cannot be empty.";
            return;
        }

        if (int.TryParse(TextEditID.text, out id))
        {
            btnID.enabled = false;
            output.text = "";
            StartCoroutine("GetUserData");
        }
        else
        {
            output.text = "Must be correct ID.";
        }
    }

    IEnumerator GetUserData()
    {
        WWWForm form = new WWWForm();
        form.AddField("user_id", int.Parse(TextEditID.text)); // 69249494
        form.AddField("fields", "first_name, last_name, photo_max_orig");
        form.AddField("v", "5.44");
        WWW www = new WWW("https://api.vk.com/method/users.get", form);
        yield return www;
        userData = UserData.CreateFromJSON(www.text);
        if (userData.response[0].photo_max_orig != null)
        {
            StartCoroutine("SetPhoto");
        }
        else
        {
            btnID.enabled = true;
            Debug.LogError("userData.response[0].photo_max_orig = null");
            output.text = "Incorrect ID.";
        }
    }

    IEnumerator SetPhoto()
    {
        string url = userData.response[0].photo_max_orig;
        //string url = "https://pp.vk.me/c628316/v628316494/34ea1/9WzCbvbq29w.jpg";
        WWW www = new WWW(url);
        yield return www;
        Renderer renderer = GetComponent<Renderer>();
        renderer.material.mainTexture = www.texture;
        btnID.enabled = true;
        transform.localScale = Vector3.one;
        float x = transform.localScale.x * (float)www.texture.width / (float)www.texture.height;
        float y = transform.localScale.y;
        float z = transform.localScale.z;
        transform.localScale = new Vector3(x, y, z);

        // Set Full Name
        txtFullName.text = userData.response[0].first_name + " " + userData.response[0].last_name;
    }
}
