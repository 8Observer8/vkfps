﻿using UnityEngine;
using System;

[Serializable]
public class UserData
{
    public Response[] response;

    public static UserData CreateFromJSON(string jsonString)
    {
        return JsonUtility.FromJson<UserData>(jsonString);
    }
}

[Serializable]
public class Response
{
    public int id;
    public string first_name;
    public string last_name;
    public string photo_max_orig;
}